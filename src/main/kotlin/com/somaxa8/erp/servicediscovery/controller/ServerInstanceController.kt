package com.somaxa8.erp.servicediscovery.controller

import org.springframework.cloud.client.ServiceInstance
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ServerInstanceController(
    private val _discoveryClient: DiscoveryClient,
) {

    @RequestMapping("/service-instances/{applicationName}")
    fun serviceInstancesByApplicationName(@PathVariable applicationName: String): List<ServiceInstance> {
        return _discoveryClient.getInstances(applicationName)
    }

}